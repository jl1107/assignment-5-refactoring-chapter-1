import java.util.ArrayList;

public class StatementData {
    public final String customer;
    public final ArrayList<Performance> performances;
    private int totalAmount;
    private int totalVolumeCredits;

    public StatementData(String customer, ArrayList<Performance> performances) {
        this.customer = customer;
        this.performances = performances;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalVolumeCredits(int totalVolumeCredits) {
        this.totalVolumeCredits = totalVolumeCredits;
    }

    public int getTotalVolumeCredits() {
        return totalVolumeCredits;
    }
}
